gsmartcontrol (1.1.3-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Archive, Repository.
  * Update standards version to 4.1.5, no changes needed.

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:56:11 +0200

gsmartcontrol (1.1.3-2) unstable; urgency=medium

  * Migrate to Salsa.
  * Update spelling fixes.
  * Switch to debhelper compatibility level 11.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 07 May 2018 09:23:22 +0200

gsmartcontrol (1.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Move to Priority “optional”.
  * Standards-Version 4.1.1, no further change required.
  * Enable building without fakeroot.

 -- Stephen Kitt <skitt@debian.org>  Thu, 16 Nov 2017 09:10:59 +0100

gsmartcontrol (1.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 01 Oct 2017 20:32:15 +0200

gsmartcontrol (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * The “Perform Tests” tab has been merged with the “Self-Tests” tab, and
    vertical space is used appropriately (at least, if there are test
    results). Closes: #641564.
  * Upstream gsmartcontrol-root now supports pkexec, use that instead of
    our su-to-root-based script. LP: #1713311.
  * Stop shipping TODO, it’s not much use for end-users.
  * Switch to debhelper compatibility level 10.
  * Standards-Version 4.1.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 11 Sep 2017 09:22:25 +0200

gsmartcontrol (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Handle Finnish separators; thanks to Aapo Rantalainen for the patch!
    LP: #1463108.
  * Fix a spelling mistake and override a Lintian spelling false-
    positive.

 -- Stephen Kitt <skitt@debian.org>  Mon, 21 Aug 2017 09:42:52 +0200

gsmartcontrol (1.0.1-2) unstable; urgency=medium

  * The dependency on menu is needed for su-to-root, restore it.
  * Acknowledge the 0.8.7-1.2 NMU. Closes: #837546.
  * Fix quoting in gsmartcontrol-root. Closes: #723973.

 -- Stephen Kitt <skitt@debian.org>  Mon, 10 Jul 2017 21:47:49 +0200

gsmartcontrol (1.0.1-1) unstable; urgency=medium

  * New upstream release. Closes: #864538. Obsoletes
    compat_with_smartmontools_6.patch and 05_fix_ATA_version.patch.
  * Use dh-autoreconf and stop patching Makefile.in.
  * Adopt the package; thanks to Giuseppe for maintaining it previously!
    Closes: #866834.
  * Update watch file.
  * Clean up debian/control with cme.
  * Drop the old menu support.
  * Drop obsolete README.source.
  * Clean up SUBDIRS declarations to allow building twice in a row.
  * Rewrite debian/copyright.
  * Standards-Version 4.0.0, no further change required.
  * Move the repository to collab-maint.

 -- Stephen Kitt <skitt@debian.org>  Mon, 10 Jul 2017 21:22:30 +0200

gsmartcontrol (0.8.7-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Use DEB_BUILD_MAINT_OPTIONS with short-form dh instead of
    hardening-wrapper. Closes: #836628.
  * Update the homepage. Closes: #769580.
  * Pull in 05_fix_ATA_version.patch from Ubuntu (thanks to Bart P.). LP:
    #1310282.

 -- Stephen Kitt <skitt@debian.org>  Mon, 12 Sep 2016 13:42:12 +0200

gsmartcontrol (0.8.7-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Handle empty sections in recent versions of smartmontools; thanks to
    Francesco Presel for the patch. Closes: #722989.

 -- Stephen Kitt <skitt@debian.org>  Fri, 12 Sep 2014 08:40:12 +0200

gsmartcontrol (0.8.7-1) unstable; urgency=low

  * [314881d] Updated debian/watch
  * [18ebada] Imported Upstream version 0.8.7
  * [c2a1f1b] debian/rules: Provide build-arch and build-indep
  * [d3036a4] Enabled Hardening Options
  * [2edfb87] Refreshed patches and removed patches apllied upstream
  * [ac3b953] Bump to standard versions 3.9.4
  * [292c276] Remove quilt from depends

 -- Giuseppe Iuculano <iuculano@debian.org>  Fri, 31 May 2013 11:41:52 +0200

gsmartcontrol (0.8.6-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "ftbfs with GCC-4.7": add patch 05_gcc-4.7.patch from Paul Tagliamonte
    (adds this-> qualifier).
    Closes: #667194

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Apr 2012 14:32:59 +0200

gsmartcontrol (0.8.6-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS with glib 2.32": add patch 04_glib2.31.patch from Alexander
    Shaduri. (Closes: #665677, LP: #935155

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Apr 2012 14:19:47 +0200

gsmartcontrol (0.8.6-1) unstable; urgency=low

  * [dbb993d] Updated my email address and removed DM-Upload-Allowed
    control field
  * [b681b5b] Imported Upstream version 0.8.6
  * [ab9bb7a] Refreshed patches
  * [a909506] Bump to Standards-Version 3.9.2, no changes needed
  * [48dd13d] Switch to dpkg-source 3.0 (quilt) format

 -- Giuseppe Iuculano <iuculano@debian.org>  Fri, 15 Jul 2011 14:59:29 +0200

gsmartcontrol (0.8.5-2) unstable; urgency=low

  * [8240961] Add menu in Depends (Closes: #548232) (LP: #438394)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Tue, 29 Sep 2009 23:04:43 +0200

gsmartcontrol (0.8.5-1) unstable; urgency=low

  * [7f4b7f6] Imported Upstream version 0.8.5
  * [51d2a10] Refreshed patches
  * [8c6daef] Updated to Standards-Version 3.8.3 (no changes needed)
  * [30bc489] Added a README.source

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Tue, 15 Sep 2009 09:52:00 +0200

gsmartcontrol (0.8.4-2) unstable; urgency=low

  * [d6e1ccc] debian/patches/03_gcc4.4.patch: Added a missing include
    and fix FTBFS with GCC 4.4 (Closes: #525734)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Mon, 11 May 2009 12:43:43 +0200

gsmartcontrol (0.8.4-1) unstable; urgency=low

  * [56eb25b] Fix a typo in the previous changeleg entry
  * [74046f6] New Upstream Version 0.8.4
  * [8fdb261] debian/patches/03_blacklist_md_devices.patch: Removed
  * [bdbe2af] debian/rules: Do not install debian/gsmartcontrol.1, now
    it is in upstream
  * [62c0daa] Updated watch file
  * [bc779d0] debian/rules: use dh_auto_configure
  * [07120d7] Updated to standards version 3.8.1 (No changes needed)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Thu, 26 Mar 2009 23:18:40 +0100

gsmartcontrol (0.8.3-3) unstable; urgency=low

  * [7468f7c] debian/control: Fix Vcs-Browser field
  * [5c94558] debian/gsmartcontrol.1: gsmartcontrol supports quite a
    number of options, added to manpage. Thanks Alexander Shaduri
  * [f8e620b] debian/gsmartcontrol-root: The gsmartcontrol-root script
    was incompatible with the original one, which uses the first
    argument for desktop selection. Fixed, thanks Alexander Shaduri
  * [10861d9] debian/copyright: gsmartcontrol is released under either
    GPL 2 or GPL 3 but not any other version, so point to both of the
    specific versions of the GPL that the package license references and
    fix copyright-refers-to-versionless-license-file lintian warning.

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Wed, 04 Feb 2009 12:18:06 +0100

gsmartcontrol (0.8.3-2) unstable; urgency=low

  * [2cc4a61] debian/rules: Do not use --enable-optimize-options in
    configure, and fix FTBFS

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Sun, 01 Feb 2009 12:13:20 +0100

gsmartcontrol (0.8.3-1) unstable; urgency=low

  * Initial release (Closes: #508512)
  * debian/patches/01_use_su-to-root.patch: use su-to-root instead of upstream
    gsmartcontrol-root script
  * debian/patches/02_fix_doc_install.patch: Do not install doc and license
    files
  * debian/patches/03_blacklist_md_devices.patch: do not scan /dev/md[0-9]
    devices

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Mon, 29 Dec 2008 09:57:37 +0100
